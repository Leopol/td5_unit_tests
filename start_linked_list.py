#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 2021

Main file for launching LinkedList functions
"""
from __future__ import absolute_import
from linked_list import LinkedList
from linked_list import Node


def func_get():
    """ For the get() function of class LinkedList
    If the list is empty (l), the function returns an exception message
    """
    empty = LinkedList([])
    empty.get(0)
    print(llist.get(3))


def func_add_last():
    """ For the add_last() function of class LinkedList """
    print("Adding a node at the end:")
    llist.add_last(Node("g"))
    print(str(llist) + "\n")


def func_add_first():
    """ For the add_first() function of class LinkedList """
    print("Adding a node at the beginning:")
    llist.add_first(Node("z"))
    print(str(llist) + "\n")


def func_add_before():
    """ For the add_before() function of class LinkedList """
    print("Adding a node before a given node:")
    print("Addition of a node 'y' before a given node 'z':")
    llist.add_before("z", Node("y"))
    print(llist)
    print("Addition of a node 'f' before a given node 'g':")
    llist.add_before("g", Node("f"))
    print(str(llist) + "\n")


def func_add_after():
    """ For the add_after() function of class LinkedList """
    print("Adding a node after a given node:")
    print("Addition of a node 'h' after a given node 'g':")
    llist.add_after("g", Node("h"))
    print(llist)
    print("Addition of a node 'u' after a given node 'a':")
    llist.add_after("a", Node("u"))
    print(str(llist) + "\n")


def func_remove():
    """ For the remove_node() function of class LinkedList """
    print("Removing a node:")
    print("Deleting a node chooses 'f':")
    llist.remove_node("f")
    print(llist)
    print("Deleting a node chooses 'a':")
    llist.remove_node("a")
    print(str(llist) + "\n")


def func_recur():
    """ For the recursion() function of class LinkedList """
    print("Recursion:")
    # We want the 3rd element from the start of the linked list
    print(str(llist.recursion(3, llist.head)) + "\n")


if __name__ == "__main__":
    print("Starting Character linked list:")
    llist = LinkedList(["a", "b", "c", "d", "e"])
    print(str(llist) + "\n")
    func_recur()
    #func_get()
    func_add_last()
    func_add_first()
    func_add_before()
    func_add_after()
    func_remove()
