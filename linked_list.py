#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 2021

Class Node & LinkedList
Definition of the methods that can be done on a linked list of nodes:
- Insertion (before, after, at the beginning, at the end)
- Deletion
- Format display
"""

class Node:
    """ Node of a list """

    def __init__(self, data):
        """
        Initialization function which constructs/characterizes
        a node (data & link)
        """
        self.data = data
        self.next = None


    def __repr__(self):
        """ Allows to return the values ​​of the nodes """
        return self.data


class LinkedList:
    """ Linked list """

    def __init__(self, nodes=None):
        """
        Initialization function which constructs/characterizes
        the linked list
        """
        # Assignment to an undefined variable
        self.head = None
        # If the nodes are not undefined and the length is different from 0
        if nodes is not None and len(nodes) != 0:
            # We establish a new variable which calls on
            # the Node class and builds the nodes to form a linked list
            node = Node(data=nodes.pop(0))
            self.head = node
            # Determination that for an element (node) in the linked list,
            # the next node follows the Node characteristic (data and next)
            # and this for all the nodes.
            for elem in nodes:
                node.next = Node(data=elem)
                node = node.next


    def get(self, index):
        """
        If the base node (the root) is empty, an exception is made.
        We would like this function to do the recursion.
        """
        # If the first node is undefined
        if self.head is None:
            # We are raising an exceptional message
            raise Exception("The list is empty, the node doesn't exist")
        # Otherwise, the recursion is done and displays the linked list
        self.recursion(index, self.head)


    def recursion(self, index, node):
        """
        Recursion is a function that is called/comes into play
        automatically, repeatedly (cnrtl.fr).
        In recursion, don't forget to return !
        """
        # Shows us the index and the node chooses to start the recursion
        print(index, node)
        # If the node is not defined
        if node is None:
            # We return the node
            return node
        # If the index has the value and equals 0
        if index == 0:
            # We return the node
            return node
        # Otherwise, we do the recursion for the before index
        # as well as the before node in order to browse the linked list
        return self.recursion(index-1, node.next)


    def add_after(self, data, new_node):
        """ Function that adds a node after a defined node """
        # If there is no head node
        if not self.head:
            # Exception message raised
            raise Exception("List is empty")
        # For a node in the list
        for node in self:
            # If the data of the node corresponds to the searched node
            if node.data == data:
                # We assign the link of the new node to the node after
                new_node.next = node.next
                # And the link from the sought node to the new node
                node.next = new_node
                return(self, data, new_node)
        # Display according to a certain format of a message if the data of
        # the searched node does not exist/the searched node does not exist
        raise Exception("Node with data '{}' not found".format(data))


    def add_before(self, data, new_node):
        """ Function that adds a node before a defined node """
        # If there is no head node
        if not self.head:
            # Exception message raised
            raise Exception("List is empty")
        # If the data sought corresponds to the data of the head node
        if self.head.data == data:
            # We apply the add_first() function on the new node we want to add
            return self.add_first(new_node)
        # Assigning a variable to the head node
        prev_node = self.head
        # For a node in the list
        for node in self:
            # If the data of the node corresponds to the searched node
            if node.data == data:
                # We assign the link of the old head node to the new node
                prev_node.next = new_node
                # And the link from the new node to an other node
                new_node.next = node
                return(data, new_node)
            prev_node = node
        # Display according to a certain format of a message if the data of
        # the searched node does not exist/the searched node does not exist
        raise Exception("Node with data '{}' not found".format(data))


    def remove_node(self, data):
        """ This function allows you to delete a given node """
        # If there is no head node
        if not self.head:
            # Exception message raised
            raise Exception("List is empty")
        # If the data sought corresponds to the data of the head node
        if self.head.data == data:
            # The head node becomes the next node
            self.head = self.head.next
            return
        # Assigning a variable to the head node
        previous_node = self.head
        # # For a node in the list
        for node in self:
            # If the data of the node corresponds to the searched node
            if node.data == data:
                # The tie of the head node becomes the tie of the next node
                previous_node.next = node.next
                return(self, data)
            previous_node = node
        # Display according to a certain format of a message if the data of
        # the searched node does not exist/the searched node does not exist
        raise Exception("Node with data '{}' not found".format(data))


    def add_first(self, node_to_add):
        """ This function adds a node as the head of the chained list """
        # The next node of the added node is the head node
        node_to_add.next = self.head
        # The head node becomes the new added node
        self.head = node_to_add
        return(self)


    def add_last(self, node_to_add):
        """ This function adds a node as the last node in the chained list """
        # If the head node is set to None and is not defined
        if self.head is None:
            # the newly added node becomes the last node
            self.head = node_to_add
            # we return this node
            return
        # Assigning a "node" variable to the head node
        node = self.head
        # As long as the next node at the head node is not undefined
        # (is defined), the node browses the chained list from node to node
        # Other order: while node.next is not None:*
        while node.next is not None:
            node = node.next
        # And when the next node is not defined, it becomes the new node added
        node.next = node_to_add


    def __repr__(self):
        """
        This function is used here to display the linked list
        in a certain format.
        """
        # Assignment of a variable to the head node
        node = self.head
        # Creating an empty list
        nodes = []
        # As long as the variable is defined
        while node is not None:
            # The list is updated taking into account the data of the nodes
            # as well as the links to the following
            nodes.append(node.data)
            node = node.next
        # returns as "a"
        return "{}".format(nodes)


    def __iter__(self):
        """
        Iteration function which allows by a repetitive calculation
        (while loop) to find the root, to build the linked list.
        """
        # Assignment of a variable to the head node
        node = self.head
        # As long as the variable is defined
        while node is not None:
            # Trad: yield = rendement
            # node-to-node path
            yield node
            node = node.next
