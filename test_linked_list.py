#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 2021

TP INLO Software test: Handling of unit tests
"""
from __future__ import absolute_import
import copy
import unittest
from linked_list import LinkedList
from linked_list import Node

class TestLinkedList(unittest.TestCase):
    """ Testing "linkedList" functions with the unittest library """

    def setUp(self):
        """ Initialization of the tests """
        self.empty_list = LinkedList([])
        self.original_list = LinkedList([1, 5, 9, 16, 27])
        self.completed_list = copy.deepcopy(self.original_list)


    def test_check_empty_list(self):
        """ 1.2.1
        This function checks that the "get()" function raises
        the expected exception: that the list is empty.
        """
        self.assertRaises(Exception, LinkedList.get, self.empty_list, 0)


    def test_add_elem(self):
        """ 1.2.2
        We check here that when adding an element, the length/size
        of the list must be different from 0.
        --> With the multiple ways to add a new node:
        """
        ##Add_first()
        #print(self.empty_list)
        self.assertIsNot(self.empty_list.add_first(Node("one")), [])
        #print("First:" + str(self.empty_list))

        ##Add_last()
        self.assertNotEqual(self.empty_list.add_last(Node("four")), [])
        #print("Latest:" + str(self.empty_list))

        ##Add_before()
        self.assertNotEqual(self.completed_list.add_before(9, Node("two")), [])
        #print("Before:" + str(self.completed_list))

        ##Add_after()
        self.assertNotEqual(self.completed_list.add_after(16, Node("three")), [])
        #print("After:" + str(self.completed_list))


    def test_unchanged_list(self):
        """ 1.2.3
        One can wonder if the change of the list comes from its
        content or from its length (number of elements).
        """
        ## --> On the content: we add and delete the same node
        ##The different ways to add a node:
        #LinkedList.add_first(self.completed_list, Node('two'))
        #LinkedList.add_last(self.completed_list, Node('two'))
        #LinkedList.add_before(self.completed_list, 9, Node('two'))
        LinkedList.add_after(self.completed_list, 9, Node('two'))
        #print(self.completed_list)
        LinkedList.remove_node(self.completed_list, 'two')
        #print(self.completed_list)
        #print(self.original_list)
        self.assertEqual(self.completed_list.__repr__(), self.original_list.__repr__())
        ##Other order:
        #self.assertEqual(str(self.completed_list), str(self.original_list))

        ## --> Along the length: we add a knot and remove another
        LinkedList.add_after(self.completed_list, 9, Node('two'))
        #print(self.completed_list)
        LinkedList.remove_node(self.completed_list, 16)
        #print(self.completed_list)
        #print(self.original_list)
        new_list = len([self.completed_list.__repr__()])
        origin = len([self.original_list.__repr__()])
        self.assertEqual(new_list, origin)


    def test_add_elem_first(self):
        """ 1.2.4
        This function checks that the element to add in the linked list
        is the first element.
        """
        ##Add_first()
        LinkedList.add_first(self.completed_list, Node('e'))
        #print(self.completed_list)
        #print(self.completed_list.head)
        self.assertEqual(self.completed_list.head.__repr__(), 'e')

        ##Add_before()
        LinkedList.add_before(self.completed_list, 1, Node('e'))
        #print(self.completed_list)
        #print(self.completed_list.head)
        self.assertEqual(self.completed_list.head.__repr__(), 'e')


#     def tearDown(self):
# 		""" Freeing resources, closing any files open in setUp() """


if __name__ == "__main__":
    unittest.main()
